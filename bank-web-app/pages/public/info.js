import React from 'react';
import { Row, Col } from 'antd';
import TransactionInformationTable from '../../components/info/TransactionInformation';
import TokenInfo from '../../components/TokenInfo';

function Info() {
	return (
		<>
			<Row gutter={[16, 16]}>
				<Col span={24}>
					<TransactionInformationTable />
				</Col>
				<Col span={24}>
					<TokenInfo />
				</Col>
			</Row>
		</>
	);
}

export default Info;
