import React from 'react';
import { Result, Button } from 'antd';

function BrokerConfirmationFail({ backToHome }) {
	return (
		<Result
			status="error"
			title="Broker reports the Money handover event!"
			subTitle="Broker reports the Money handover event to the Bank"
			extra={[
				<Button type="primary" key="next" onClick={(e) => backToHome(e)}>
					Back to home
				</Button>,
			]}
		/>

	);
}

export default BrokerConfirmationFail;
