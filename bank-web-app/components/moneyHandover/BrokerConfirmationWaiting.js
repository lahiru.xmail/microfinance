import React from 'react';
import { Spin, Alert, Statistic, Button, Space } from 'antd';

function BrokerConfirmationWaiting({ setIsTimesUp }) {
	// const deadline = Date.now() + 1000 * 60; //one minute
	const deadline = Date.now() + 1000 * 5;
	const { Countdown } = Statistic;

	const onFinish = () => {
		setIsTimesUp(true);
	};

	return (
		<>
			<Spin tip="Waiting..." size="large">
				<Alert
					message="Waiting for Broker's confirmation"
					description={<Countdown title="Countdown" value={deadline} onFinish={onFinish} />}
					type="info"
				/>
			</Spin>
		</>

	);
}

export default BrokerConfirmationWaiting;
