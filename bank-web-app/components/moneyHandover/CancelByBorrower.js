import React from 'react';
import { Result, Button } from 'antd';

function CancelByBorrower({ flagByBorrower }) {
	return (
		<Result
			status="error"
			title="Broker didn't confirm!"
			subTitle="Broker didn't confirm the money within given time. Inform Bank."
			extra={[
				<Button type="primary" key="next" onClick={(e) => flagByBorrower(e)}>
					Report to Bank
				</Button>,
			]}
		/>

	);
}

export default CancelByBorrower;
