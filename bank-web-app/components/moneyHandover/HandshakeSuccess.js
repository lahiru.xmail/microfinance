import React from 'react';
import { Result, Button } from 'antd';

function HandshakeSuccess({ next, cancelByBorrower }) {
	return (
		<Result
			status="success"
			title="Broker accept the handshake!"
			subTitle="you can handover money to broker"
			extra={[
				<Button type="primary" key="next" onClick={(e) => next(e)}>
					Next
				</Button>,
				<Button key="buy" onClick={(e) => cancelByBorrower(e)}>Stop process</Button>,
			]}
		/>

	);
}

export default HandshakeSuccess;
