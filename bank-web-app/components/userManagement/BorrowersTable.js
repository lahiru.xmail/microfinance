import React, { useState, useContext, useEffect } from 'react';
import { Table, Tag, Card, Divider, message, Modal, Image } from 'antd';
import AuthContext from '../../stores/authContext';

function BorrowersTable() {
	const { user, login, BankPlans, BankLoan, UserIdentity } = useContext(AuthContext);

	const [isModalVisible, setIsModalVisible] = useState(false);
	const [isDocumentsModalVisible, setIsDocumentsModalVisible] = useState(false);
	const [id, setId] = useState(-1);
	const [currentRecord, setCurrentRecord] = useState('');

	// const showModal = (value) => {
	// 	setId(value);
	// 	setIsModalVisible(true);
	// };

	// const handleOk = () => {
	// 	setIsModalVisible(false);
	// };

	const handleCancel = () => {
		setIsModalVisible(false);
		setIsDocumentsModalVisible(false);
	};

	const showDocumentsModal = (record) => {
		setId(record.id);
		setCurrentRecord(record);
		setIsDocumentsModalVisible(true);
	};

	const handleDocumentsModalOk = () => {
		setIsDocumentsModalVisible(false);
	};

	const approveLoan = async (loanId) => {
		try {
			const accounts = await window.ethereum.enable();
			// console.log(accounts[0]);
			await BankLoan.methods.approveLoan(loanId).send({ from: accounts[0] });
			message.success(`Loan ${loanId} approved`);
		} catch (err) {
			message.error('Error creating new plan');
		}
	};

	const columns = [
		{
			title: 'ID',
			dataIndex: 'id',
			key: 'id',
			render: text => text,
		},
		{
			title: 'Social Id',
			dataIndex: 'socialId',
			key: 'socialId',
		},
		{
			title: 'Name',
			dataIndex: 'name',
			key: 'name',
		},
		{
			title: 'Address',
			dataIndex: 'address',
			key: 'address',
		},
		{
			title: 'Added by',
			dataIndex: 'addedBy',
			key: 'addedBy',
		},
		{
			title: 'Document',
			key: '',
			dataIndex: '',
			render: (record) => (
				<span>
					<a href="javascript:void(0);" onClick={() => showDocumentsModal(record)}>View</a>
				</span>
			),
		},
		{
			title: 'Status',
			key: 'status',
			dataIndex: 'status',
			render: tag => {
				/* PENDING, APPROVED, REJECTED */
				let color = '';
				let state = '';
				if (tag === '0') {
					color = 'geekblue';
					state = 'PENDING';
				} else if (tag === '1') {
					color = 'green';
					state = 'APPROVED';
				} else {
					color = 'red';
					state = 'REJECTED';
				}
				return (
					<Tag color={color} key={tag}>
						{state}
					</Tag>
				);
			},
		},
	];

	if (user.role === 'bank') {
		columns.push({
			title: 'Action',
			dataIndex: '',
			key: 'x',
			render: (record) => (
				record.status === '0' ?
					<span>
						<a href="javascript:void(0);" onClick={() => approveLoan(record.id)}>Approve</a>
						<Divider type="vertical" />
						<a href="javascript:;" style={{ color: 'red' }}>Reject</a>
					</span> : null
			),
		});
	}

	const [data, setData] = useState([]);
	const brokers = {};

	// const getBrokers = async () => {
	// 	const response = await UserIdentity.methods.getAllBrokers().call();
	// 	for (let i = 0; i < response.length; i++) {
	// 		brokers[response[i].userAddress] = response[i].name;
	// 	}
	// };

	const getBorrowers = async () => {
		try {
			const response = await UserIdentity.methods.getAllBorrowers().call();

			setData([]);

			for (let i = 0; i < response.length; i++) {
				const row = {
					key: response[i].id,
					id: response[i].id,
					socialId: response[i].socialSecurityId,
					address: response[i].userAddress,
					name: response[i].name,
					addedBy: brokers[response[i].addedBy],
					documentsUri: response[i].documentsUri,
					status: response[i].state,
				};

				console.log(row);

				setData((prev) => {
					return [...prev, row];
				});
			}
			// console.log(response);
		} catch (err) {
			console.log(err);
			message.error('Error occured while loading brokers');
		}
	};

	useEffect(() => {
		getBorrowers();

		// const emitter = BankLoan.events.loanRequest({ fromBlock: 'latest' }, (error, response) => {
		// 	// console.log(data.returnValues);
		// 	const result = response.returnValues;

		// 	const row = {
		// 		key: result.id,
		// 		id: result.id,
		// 		amount: result.amount,
		// 		period: result.months,
		// 		interest: result.interest,
		// 		planId: result.planId,
		// 		borrower: result.borrower,
		// 		broker: result.broker,
		// 		insurance: result.insurance,
		// 		InsurancePolicyId: result.InsurancePolicyId,
		// 		status: result.state,
		// 	};

		// 	// console.log(row)

		// 	setData((prev) => {
		// 		return [...prev, row];
		// 	});
		// });

		// return () => {
		// 	emitter.unsubscribe();
		// };
	}, []);

	return (
		<>
			<Card title="Borrowers">
				<Table pagination={true} columns={columns} dataSource={data} />
			</Card>
			<Modal title={`Documents - User Id  ${id}`} visible={isDocumentsModalVisible} onOk={handleDocumentsModalOk} onCancel={handleCancel}>
				<Image
					width={400}
					src={`https://ipfs.io/ipfs/${currentRecord.documentsUri}`}
					preview={false}
				/>
			</Modal>
		</>
	);
}

export default BorrowersTable;
