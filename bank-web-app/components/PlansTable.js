import React, { useState, useContext, useEffect } from 'react';
import { Table, Form, InputNumber, Card, Divider, Modal, Button, message } from 'antd';
import { CloseCircleOutlined } from '@ant-design/icons';
import AuthContext from '../stores/authContext';

function PlansTable({ togglePlan }) {
	const { user } = useContext(AuthContext);
	const [componentSize, setComponentSize] = useState('default');
	const [isModalVisible, setIsModalVisible] = useState(false);
	const [editId, setEditId] = useState(-1);
	const [deleteId, setDeleteId] = useState(-1);
	const [data, setData] = useState([]);

	const onFormLayoutChange = ({ size }) => {
		setComponentSize(size);
	};

	const { confirm } = Modal;

	const [id, setId] = useState('');
	const [minAmount, setMinAmount] = useState('');
	const [maxAmount, setMaxAmount] = useState('');
	const [minMonths, setMinMonths] = useState('');
	const [maxMonths, setMaxMonths] = useState('');
	const [interest, setInterest] = useState('');

	const fetchPlans = async () => {
		const plansResponse = await fetch('http://localhost:9091/loan-plans');
		const plans = await plansResponse.json();
		setData([]);

		for (let i = 0; i < plans.length; i++) {
			const row = {
				key: plans[i]._id,
				id: plans[i]._id,
				minAmount: plans[i].minAmount,
				maxAmount: plans[i].maxAmount,
				minMonths: plans[i].minMonths,
				maxMonths: plans[i].maxMonths,
				interest: plans[i].interest,
			};

			setData((prev) => {
				return [...prev, row];
			});
		}
	};

	const fetchPlanById = async (planId) => {
		const plansResponse = await fetch('http://localhost:9091/loan-plans/' + planId);
		const plan = await plansResponse.json();
		// console.log(plans);

		setId(plan._id);
		setMinAmount(plan.minAmount);
		setMaxAmount(plan.maxAmount);
		setMinMonths(plan.minMonths);
		setMaxMonths(plan.maxMonths);
		setInterest(plan.interest);
	};

	const showModal = (value) => {
		setEditId(value);
		setIsModalVisible(true);
		fetchPlanById(value);
	};

	const deletePlan = (planId) => {
		setDeleteId(planId);
		confirm({
			icon: <CloseCircleOutlined style={{ color: 'red' }} />,
			content: `Delete Loan Plan ${planId}`,
			onOk: async () => {
				// confirmDeleteLoanPlan(planId);
				try {
					console.log(deleteId);
					await fetch('http://localhost:9091/loan-plans/' + deleteId,
						{ method: 'DELETE' },
					);
					console.log(deleteId);
					message.success('Sucsessfully delete the loan plan');
					fetchPlans();
				} catch (err) {
					console.log(err);
					message.error('Error occured while deleting loan plan');
				}
			},
			onCancel() {
				console.log('Cancel');
			},
		});
	};

	const columns = [
		{
			title: 'ID',
			dataIndex: 'id',
			key: 'id',
			render: text => text,
		},
		{
			title: 'Min Amount',
			dataIndex: 'minAmount',
			key: 'minAmount',
		},
		{
			title: 'Max Amount',
			dataIndex: 'maxAmount',
			key: 'maxAmount',
		},
		{
			title: 'Minimum Period',
			dataIndex: 'minMonths',
			key: 'minMonths',
			render: text => text + ' months',
		},
		{
			title: 'Maximum Period',
			dataIndex: 'maxMonths',
			key: 'maxMonths',
			render: text => text + ' months',
		},
		{
			title: 'Interest %',
			key: 'interest',
			dataIndex: 'interest',
		},
	];

	if (user.role === 'bank') {
		columns.push({
			title: 'Action',
			dataIndex: '',
			key: 'x',
			render: (record) => (
				<span>
					<a href="javascript:void(0);" onClick={() => showModal(record.id)}>Edit</a>
					<Divider type="vertical" />
					<a href="javascript:void(0);" onClick={() => deletePlan(record.id)} style={{ color: 'red' }}>Delete</a>
				</span>
			),
		});
	}

	const handleOk = async () => {
		setIsModalVisible(false);
		try {
			const editResponse = await fetch('http://localhost:9091/loan-plans/' + id, {
				method: 'PATCH',
				headers: { 'Content-type': 'application/json' },
				body: JSON.stringify({
					minAmount,
					maxAmount,
					minMonths,
					maxMonths,
					interest,
				}),
			});
			message.success('Loan Plan changed successfully');
			fetchPlans();
		} catch (err) {
			message.error('Error while editting the Loan Plan');
		}
	};

	const handleCancel = () => {
		setIsModalVisible(false);
	};

	useEffect(() => {
		fetchPlans();
	}, []);

	useEffect(() => {
		fetchPlans();
	}, [togglePlan]);

	return (
		<>
			<Card
				title="Loan Plans"
				extra={<a href="javascript:void(0);" onClick={() => fetchPlans()}>Refresh</a>}
			>
				<Table columns={columns} dataSource={data} />
			</Card>
			<Modal
				title="Edit Loan Plan"
				visible={isModalVisible}
				onOk={handleOk}
				onCancel={handleCancel}
				footer={[
					<Button key="back" onClick={handleCancel}>
						Cancel
					</Button>,
					<Button key="submit" type="primary" onClick={handleOk}>
						Save Changes
					</Button>,
				]}
			>
				<Form
					labelCol={{
						span: 5,
					}}
					wrapperCol={{
						span: 18,
					}}
					layout="horizontal"
					initialValues={{
						size: componentSize,
					}}
					onValuesChange={onFormLayoutChange}
					size={componentSize}
				>
					<Form.Item label="Id">
						<span className="ant-form-text">{id}</span>
					</Form.Item>
					<Form.Item label="Min amount">
						<InputNumber
							min="0"
							style={{ width: '100%' }}
							placeholder="Enter amount"
							value={minAmount}
							onChange={(e) => setMinAmount(e)}
						/>
					</Form.Item>
					<Form.Item label="Max amount">
						<InputNumber
							min="0"
							style={{ width: '100%' }}
							placeholder="Enter amount"
							value={maxAmount}
							onChange={(e) => setMaxAmount(e)}
						/>
					</Form.Item>
					<Form.Item label="Min months">
						<InputNumber
							min="0"
							style={{ width: '100%' }}
							placeholder="Enter deal period"
							value={minMonths}
							onChange={(e) => setMinMonths(e)}
						/>
					</Form.Item>
					<Form.Item label="Max months">
						<InputNumber
							min="0"
							style={{ width: '100%' }}
							placeholder="Enter deal period"
							value={maxMonths}
							onChange={(e) => setMaxMonths(e)}
						/>
					</Form.Item>
					<Form.Item label="Interest">
						<InputNumber
							min="0"
							style={{ width: '100%' }}
							placeholder="Enter interes rate"
							value={interest}
							onChange={(e) => setInterest(e)}
						/>
					</Form.Item>
				</Form>
			</Modal>
		</>
	);
}

export default PlansTable;
