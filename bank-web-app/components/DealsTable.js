import React from 'react';
import { Table, Tag, Space, Card } from 'antd';

const columns = [
	{
		title: 'ID',
		dataIndex: 'id',
		key: 'id',
		render: (text) => text,
  },
  {
    title: "Amount",
    dataIndex: "amount",
    key: "amount",
  },
  {
    title: "Period",
    dataIndex: "period",
    key: "period",
  },
  {
    title: "Interest %",
    key: "interest",
    dataIndex: "interest",
  },
  {
    title: "Borrower",
    key: "borrower",
    dataIndex: "borrower",
    render: (text, record) => (
      <Space size='middle'>
        8263746
        <Tag color='geekblue' key='Accepted'>
          {"Accepted".toUpperCase()}
        </Tag>
      </Space>
    ),
  },
  {
    title: "Insurer",
    key: "insurer",
    dataIndex: "insurer",
    render: (text, record) => (
      <Space size='middle'>
        8263746
        <Tag color='geekblue' key='Accepted'>
          {"Accepted".toUpperCase()}
        </Tag>
      </Space>
    ),
  },
  {
    title: "Insurer Fee",
    key: "fee",
    dataIndex: "fee",
  },
];

const data = [
  {
    key: "1",
    id: "1",
    name: "John Brown",
    amount: "100",
    period: "10 months",
    interest: 13,
    fee: 2,
    tags: ["pending"],
  },
  {
    key: "2",
    id: "2",
    name: "Jim Green",
    amount: "200",
    period: "18 months",
    interest: 12,
    fee: 3,
    tags: ["approved"],
  },
  {
    key: "3",
    id: "3",
    name: "Joe Black",
    amount: "400",
    period: "24 months",
    interest: 10,
    fee: 4,
    tags: ["rejected"],
  },
];

function DealsTable() {
  return (
    <>
      <Card title='Current Deals' style={{ marginLeft: "12px" }}>
        <Table pagination={false} columns={columns} dataSource={data} />
      </Card>
    </>
  );
}

export default DealsTable;
