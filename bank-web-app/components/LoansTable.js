import React, { useState, useContext, useEffect } from 'react';
import { Table, Tag, Card, Divider, message, Modal } from 'antd';
import AuthContext from '../stores/authContext';

function LoansTable() {
	const { user, BankLoan } = useContext(AuthContext);

	const [isModalVisible, setIsModalVisible] = useState(false);
	const [isRejectModalVisible, setIsRejectModalVisible] = useState(false);
	const [id, setId] = useState(-1);

	const approveLoan = async () => {
		try {
			const accounts = await window.ethereum.enable();
			// console.log(accounts[0]);
			await BankLoan.methods.approveLoan(id).send({ from: accounts[0] });
			message.success(`Loan ${id} approved`);
		} catch (err) {
			message.error('Error occured while approving the loan');
		}
	};

	const rejectLoan = async () => {
		try {
			const accounts = await window.ethereum.enable();
			await BankLoan.methods.rejectLoan(id).send({ from: accounts[0] });
			message.success(`Loan ${id} rejected`);
		} catch (err) {
			message.error('Error occured while rejecting the loan');
		}
	};

	const showModal = (value) => {
		setId(value);
		setIsModalVisible(true);
	};

	const showRejectModal = (value) => {
		setId(value);
		setIsRejectModalVisible(true);
	};

	const handleOk = () => {
		approveLoan();
		setIsModalVisible(false);
	};

	const handleReject = () => {
		rejectLoan();
		setIsModalVisible(false);
	};

	const handleCancel = () => {
		setIsModalVisible(false);
		setIsRejectModalVisible(false);
	};

	const columns = [
		{
			title: 'ID',
			dataIndex: 'id',
			key: 'id',
			render: text => text,
		},
		{
			title: 'Borrower',
			dataIndex: 'borrower',
			key: 'borrower',
		},
		{
			title: 'Broker',
			dataIndex: 'broker',
			key: 'broker',
		},
		{
			title: 'Amount',
			dataIndex: 'amount',
			key: 'amount',
		},
		{
			title: 'Period',
			dataIndex: 'period',
			key: 'period',
		},
		{
			title: 'Interest %',
			key: 'interest',
			dataIndex: 'interest',
		},
		{
			title: 'Plan ID',
			key: 'planId',
			dataIndex: 'planId',
		},
		{
			title: 'Status',
			key: 'status',
			dataIndex: 'status',
			render: tag => {
				const state = ['REQUESTED', 'INSURANCE_APPLIED', 'INSURANCE_APPROVED', 'BORROWER_SIGNED',
					'INSURANCE_REJECTED', 'BANK_APPROVED', 'BANK_REJECTED', 'PAID_TO_BROKER', 'PAID_TO_INSURANCE',
					'ONGOING', 'DEFAULT', 'CLOSE', 'CLAIM_REQUESTED', 'CLAIMED'];
				let color = 'geekblue';
				if (tag === '4' || tag === '6' || tag === '10') {
					color = 'red';
				} else if (tag === '5' || tag === '9') {
					color = 'green';
				}
				return (
					<Tag color={color} key={tag}>
						{state[tag]}
					</Tag>
				);
			},
		},
	];

	if (user.role === 'bank') {
		// columns.splice(1, 0, {
		// 	title: 'Name',
		// 	dataIndex: 'name',
		// 	key: 'name',
		// });

		columns.push({
			title: 'Action',
			dataIndex: '',
			key: 'x',
			render: (record) => (
				record.status === '4' ?
					<span>
						<a href="javascript:void(0);" onClick={() => showModal(record.id)}>Approve</a>
						<Divider type="vertical" />
						<a href="javascript:;" onClick={() => showRejectModal(record.id)} style={{ color: 'red' }}>Reject</a>
					</span> : null
			),
		});
	}

	const [data, setData] = useState([]);

	const getLoans = async () => {
		try {
			// const accounts = await window.ethereum.enable();
			// console.log(accounts[0]);
			const response = await BankLoan.methods.getLoans().call();

			setData([]);

			for (let i = 0; i < response.length; i++) {
				const row = {
					key: response[i].id,
					id: response[i].id,
					amount: response[i].amount,
					period: response[i].months,
					interest: response[i].interest,
					planId: response[i].planId,
					borrower: response[i].borrower,
					broker: response[i].broker,
					insurance: response[i].insurance,
					InsurancePolicyId: response[i].InsurancePolicyId,
					status: response[i].state,
				};

				// console.log(row);

				setData((prev) => {
					return [...prev, row];
				});
			}
			// console.log(response);
		} catch (err) {
			console.log(err);
			message.error('Error occured while loading current loans');
		}
	};

	useEffect(() => {
		getLoans();

		const emitter = BankLoan.events.loanRequest({ fromBlock: 'latest' }, (error, response) => {
			// console.log(data.returnValues);
			const result = response.returnValues;

			const row = {
				key: result.id,
				id: result.id,
				amount: result.amount,
				period: result.months,
				interest: result.interest,
				planId: result.planId,
				borrower: result.borrower,
				broker: result.broker,
				insurance: result.insurance,
				InsurancePolicyId: result.InsurancePolicyId,
				status: result.state,
			};

			// console.log(row)

			setData((prev) => {
				return [...prev, row];
			});
		});

		return () => {
			emitter.unsubscribe();
		};
	}, []);

	return (
		<>
			<Card title="Current Loans">
				<Table pagination="true" columns={columns} dataSource={data} />
			</Card>
			<Modal title={`Approve Loan Request ${id}`} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
				<p>Confirm?</p>
			</Modal>
			<Modal title={`Reject Loan Request ${id}`} visible={isRejectModalVisible} onOk={handleReject} onCancel={handleCancel}>
				<p>Reject loan request?</p>
			</Modal>
		</>
	);
}

export default LoansTable;
