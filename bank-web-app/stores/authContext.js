import React, { createContext, useState } from 'react';

import Web3 from 'web3';
import IPFS from 'ipfs-api';
import MicroTokenBuild from '../../blockchain/build/contracts/MicroToken.json';
import BankLoanBuild from '../../blockchain/build/contracts/BankLoan.json';
import UserIdentityBuild from '../../blockchain/build/contracts/UserIdentity.json';
import LoanPaymentBuild from '../../blockchain/build/contracts/LoanPayment.json';

const AuthContext = createContext({
	user: null,
	userRole: null,
	login: () => {},
});

export const AuthContextProvider = ({ children }) => {
	const users = [
		{
			name: 'Leonard Hofstadter',
			role: 'broker',
			color: '#87d068',
		},
		{
			name: 'Sheldon Cooper',
			role: 'bank',
			color: '#8193E7',
		},
		{
			name: 'Rajesh Koothrapali',
			role: 'borrower',
			color: '#8193E7',
		},
		{
			name: 'Guest',
			role: 'public',
			color: '#8193E7',
		},
	];

	const [user, setUser] = useState(users[0]);

	const login = (role) => {
		if (role === 'broker') {
			setUser(users[0]);
		} else if (role === 'bank') {
			setUser(users[1]);
		} else if (role === 'borrower') {
			setUser(users[2]);
		} else if (role === 'public') {
			setUser(users[3]);
		}
	};
	const web3 = new Web3(Web3.givenProvider || 'http://127.0.0.1:7545');

	// Smart Contract Addresses
	const microTokenAddress = MicroTokenBuild.networks[5777].address;
	const userIdentityAddress = UserIdentityBuild.networks[5777].address;
	const bankLoanAddress = BankLoanBuild.networks[5777].address;
	const loanPaymentAddress = LoanPaymentBuild.networks[5777].address;

	const UserIdentity = new web3.eth.Contract(UserIdentityBuild.abi, userIdentityAddress);
	const MicroToken = new web3.eth.Contract(MicroTokenBuild.abi, microTokenAddress);
	const BankLoan = new web3.eth.Contract(BankLoanBuild.abi, bankLoanAddress);
	const LoanPayment = new web3.eth.Contract(LoanPaymentBuild.abi, loanPaymentAddress);

	const ipfs = new IPFS({ host: 'ipfs.infura.io', port: 5001, protocol: 'https' });

	const context = { user, login, web3, ipfs, MicroToken, UserIdentity, BankLoan, LoanPayment };

	return (
		<AuthContext.Provider value={context}>
			{children}
		</AuthContext.Provider>
	);
};

export default AuthContext;
