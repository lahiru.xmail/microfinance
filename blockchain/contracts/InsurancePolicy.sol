// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "./UserIdentity.sol";

contract InsurancePolicy{
    
    enum InsuranceState{
        REQUESTED, 
        BORROWER_SIGNED,
        APPROVED, 
        REJECTED, 
        ONGOING, 
        BORROWER_PAID, 
        CLAIM_REQUESTED,
        CLAIM_APPROVED,
        CLAIM_REJECTED,
        CLAIMED, 
        CLOSE 
    }
    
    struct Payment{
        string transaction;
        uint amount;
    }
    
    struct Policy
    {
        uint id;
        uint amount;
        uint months;
        uint cover;
        uint planId;
        uint loanId;
        InsuranceState state;
        address broker;
        address borrower;
        bool insuranceApprove;
        bool isBorrowerSigned;
    }
    
    event insurancePolicyRequest(
        uint id,
        uint amount,
        uint months,
        uint cover,
        uint planId,
        uint loanId,
        InsuranceState state,
        address broker,
        address borrower,
        bool insuranceApprove,
        bool isBorrowerSigned
    );
    
    address private admin;
    UserIdentity identitySC;
    Policy[] private policies;
    
    constructor (address _identitySC) {
            admin = msg.sender;
            identitySC = UserIdentity(_identitySC);
    }
    
    modifier isAdmin()
    {
        require(msg.sender == admin);
        _;
    }
    
    modifier isBroker()
    {
        require(identitySC.verifyIsBroker(msg.sender), 'Broker Only');
        _;
    }
    
    modifier isValidInsurance(uint _insuranceId)
    {
        bool isValid = false;
        for(uint i=0; i< policies.length; i++)
        {
            if(policies[i].id == _insuranceId)
            {
                isValid = true;
                break;
            }
        }
        require(isValid);
        _;
    }
    
    function applyInsurance(uint amount, uint months, uint cover, uint planId, uint loanId, address borrower) public isAdmin()
    {
        Policy memory policy = Policy(policies.length + 1,amount, months, cover, planId, loanId,
        InsuranceState.REQUESTED, msg.sender, borrower, false, false);
        policies.push(policy);
        
        emit insurancePolicyRequest( policy.id, policy.amount, policy.months, policy.cover, policy.planId,
            policy.loanId, policy.state, policy.broker, policy.borrower, policy.insuranceApprove,
            policy.isBorrowerSigned
        );
    }

    function getAdmin() public view returns(address)
    {
        return admin;
    }
    
    // borrower
    function signContract(uint _insuranceId) public isAdmin() isValidInsurance(_insuranceId)
    {
        for (uint i = 0; i < policies.length; i++) {
            if (policies[i].id == _insuranceId && policies[i].state == InsuranceState.REQUESTED) {
                policies[i].isBorrowerSigned = true; 
                policies[i].state = InsuranceState.BORROWER_SIGNED;
                break;
            }
        }
    }
    
    function approveInsurancePolicy(uint _insuranceId) public isAdmin() isValidInsurance(_insuranceId)
    {
        for (uint i = 0; i < policies.length; i++) {
            if (policies[i].id == _insuranceId && policies[i].state == InsuranceState.BORROWER_SIGNED) {
                policies[i].insuranceApprove = true;
                policies[i].state = InsuranceState.APPROVED;
                break;
            }
        }
    }
    
    function rejectInsurancePolicy(uint _insuranceId) public isAdmin() isValidInsurance(_insuranceId)
    {
        for (uint i = 0; i < policies.length; i++) {
            if (policies[i].id == _insuranceId && policies[i].state == InsuranceState.BORROWER_SIGNED) {
                policies[i].insuranceApprove = false;
                policies[i].state = InsuranceState.REJECTED;
                break;
            }
        }
    }
    
    function confirmPaymentFromBank(uint _insuranceId) public isAdmin() isValidInsurance(_insuranceId)
    {
        for (uint i = 0; i < policies.length; i++) {
            if (policies[i].id == _insuranceId && policies[i].state == InsuranceState.APPROVED) {
                policies[i].state = InsuranceState.ONGOING;
                break;
            }
        }
    }
    
    function confirmPaymentFromBorrower(uint _insuranceId) public isAdmin() isValidInsurance(_insuranceId)
    {
        for (uint i = 0; i < policies.length; i++) {
            if (policies[i].id == _insuranceId && policies[i].state == InsuranceState.ONGOING) {
                policies[i].state = InsuranceState.BORROWER_PAID;
                break;
            }
        }
    }
    
    function closePolicy(uint _insuranceId) public isAdmin() isValidInsurance(_insuranceId)
    {
        for (uint i = 0; i < policies.length; i++) {
            if (policies[i].id == _insuranceId && 
            ( policies[i].state == InsuranceState.BORROWER_PAID || policies[i].state == InsuranceState.CLAIM_REJECTED)) {
                policies[i].state = InsuranceState.CLOSE;
                break;
            }
        }
    }
    
    function requestClaim(uint _insuranceId) public isAdmin() isValidInsurance(_insuranceId)
    {
        for (uint i = 0; i < policies.length; i++) {
            if (policies[i].id == _insuranceId && policies[i].state == InsuranceState.ONGOING) {
                policies[i].state = InsuranceState.CLAIM_REQUESTED;
                break;
            }
        }
    }
    
    function rejectClaim(uint _insuranceId) public isAdmin() isValidInsurance(_insuranceId)
    {
        for (uint i = 0; i < policies.length; i++) {
            if (policies[i].id == _insuranceId && policies[i].state == InsuranceState.CLAIM_REQUESTED) {
                policies[i].state = InsuranceState.CLAIM_REJECTED;
                break;
            }
        }
    }
    
    function approveClaim(uint _insuranceId) public isAdmin() isValidInsurance(_insuranceId)
    {
        for (uint i = 0; i < policies.length; i++) {
            if (policies[i].id == _insuranceId && policies[i].state == InsuranceState.CLAIM_REQUESTED) {
                policies[i].state = InsuranceState.CLAIM_APPROVED;
                break;
            }
        }
    }
    
    function confirmClaim(uint _insuranceId) public isAdmin() isValidInsurance(_insuranceId)
    {
        for (uint i = 0; i < policies.length; i++) {
            if (policies[i].id == _insuranceId && policies[i].state == InsuranceState.CLAIM_APPROVED) {
                policies[i].state = InsuranceState.CLAIMED;
                break;
            }
        }
    }
    
    function viewInsurancePolicy(uint _insuranceId) public view isAdmin() isValidInsurance(_insuranceId) 
    returns(uint id,
        uint amount,
        uint months,
        uint cover,
        uint planId,
        uint loanId,
        InsuranceState state,
        address broker,
        address borrower,
        bool insuranceApprove,
        bool isBorrowerSigned)
    {
        for (uint i = 0; i < policies.length; i++) {
            if (policies[i].id == _insuranceId) {
                return (policies[i].id, policies[i].amount, policies[i].months, policies[i].cover, 
                policies[i].planId, policies[i].loanId, policies[i].state, policies[i].broker, 
                policies[i].borrower, policies[i].insuranceApprove, policies[i].isBorrowerSigned);
            }
        }
        // return(0,0,0,0,0,'0x00');
    }
    
    function getAllPolicies() public view returns(Policy [] memory)
    {
        return policies;
    } 
    
    //TODO - View broker's loans
    //TODO - modify applied loans
}