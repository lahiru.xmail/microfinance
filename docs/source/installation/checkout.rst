Checking out the project
========================

After successfully install above required dependencies you can clone 
"Microfinance" project from GitLab using following command. ::

   git clone https://gitlab.com/lahiru.uno/microfinance.git

All 5 projects were placed inside the root directory. 
In the next section we discuss about the project structure and deployment.