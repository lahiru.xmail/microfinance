const Plan = require('../models/Plans')

const planService = {
	getPlans: async (req) => {
		const plans = await Plan.find()
		return plans;
	}
}
  
module.exports = planService;