import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import BasicLayout from '../components/BasicLayout'
import { useContext, useEffect } from 'react'
import { useRouter } from 'next/router'

import { Spin } from 'antd';
import AuthContext from '../stores/authContext'

export default function Home() {

  const router = useRouter();

  const { user, login} = useContext(AuthContext);

  useEffect(() => {
    {user.role == 'broker' && router.push('/broker/policy')}
    {user.role == 'bank' && router.push('/bank/plans')}
  }, [])

  
  return (
    <div style={{'margin' : 'auto'}} >
      <Spin size="large" />
    </div>
  )
}
