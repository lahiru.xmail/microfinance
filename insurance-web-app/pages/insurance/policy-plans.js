import { Row, Col } from 'antd';
import PlansTable from '../../components/PlansTable';

function Deals() {
  return (
    <Row gutter={[16, 16]}>
      <Col span={24}>
        <PlansTable></PlansTable>
      </Col>
    </Row>
  )
}

export default Deals;