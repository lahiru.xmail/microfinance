import React from 'react';
import { Row, Col } from 'antd';
import BlocksTable from '../../components/blockchain/BlocksTable';

function Blockchain() {
	return (
		<Row gutter={[16, 16]}>
			<Col span={24}>
				<BlocksTable />
			</Col>
		</Row>
	);
}

export default Blockchain;
