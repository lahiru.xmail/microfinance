import { Row, Col } from 'antd';
import DealsTable from '../../components/DealsTable'

function BorrowerDeals() {
  return (
    <Row gutter={[16, 16]}>
      <Col span={24}>
        <DealsTable></DealsTable>
      </Col>
    </Row>
  )
}

export default BorrowerDeals;