import { Row, Col } from 'antd';
import PoliciesTable from '../../components/PoliciesTable'

function Loans() {
  return (
    <Row gutter={[16, 16]}>
      <Col span={24} >
        <PoliciesTable></PoliciesTable>
      </Col>
    </Row>
  )
}

export default Loans;