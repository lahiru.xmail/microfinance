import { Row, Col } from 'antd';
import CreateInsurancePolicy from '../../components/CreateInsurancePolicy'

function ApplyPolicy() {
  return (

    <Row gutter={[16, 16]}>
      <Col span={24}>
        <CreateInsurancePolicy></CreateInsurancePolicy>
      </Col>
    </Row>
  )
}

export default ApplyPolicy;