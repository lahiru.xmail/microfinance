import 'antd/dist/antd.css';
import { Menu } from 'antd';

import { useRouter } from 'next/router';

function BrokerMenu()
{
    const router = useRouter();

    return(
        <Menu
            mode="inline"
            defaultSelectedKeys={['/req-policy']}
            // defaultOpenKeys={['sub1']}
            style={{ height: '100%', borderRight: 0 }}>
            <Menu.Item key="/req-policy" onClick={() => router.push('/broker/request-policy')}>
                Request Policy
            </Menu.Item>
            <Menu.Item key="/view-policies" onClick={() => router.push('/broker/view-policies')}>
                View Policies
            </Menu.Item>
            <Menu.Item key="/transfer" onClick={() => router.push('/public/transfer')}>
                Transfer
            </Menu.Item>
            <Menu.Item key="/blockchain" onClick={() => router.push('/public/blockchain')}>
				Blockchain
			</Menu.Item>
			<Menu.Item key="/info" onClick={() => router.push('/public/info')}>
				Info
			</Menu.Item>
        </Menu>
    )
}

export default BrokerMenu;