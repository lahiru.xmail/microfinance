import React, { useState } from 'react'
import { Table, Tag, Space, Typography, Card, Form, InputNumber, Input, Button } from 'antd';

const { Title } = Typography;

function CreatePlanForm() {

    const [componentSize, setComponentSize] = useState('default');

    const onFormLayoutChange = ({ size }) => {
        setComponentSize(size);
    };

    return (

        <Card title="Create Policy Plan" style={{ margin: '0px' }}>
            <Form
                labelCol={{
                    span: 5,
                }}
                wrapperCol={{
                    span: 18,
                }}
                layout="horizontal"
                initialValues={{
                    size: componentSize,
                }}
                onValuesChange={onFormLayoutChange}
                size={componentSize}
            >
                <Form.Item label="Min amount">
                    <InputNumber min="0" style={{ width: '100%' }} placeholder="Enter amount"/>
                </Form.Item>
                <Form.Item label="Max amount">
                    <InputNumber min="0" style={{ width: '100%' }} placeholder="Enter amount"/>
                </Form.Item>
                <Form.Item label="Min months">
                    <InputNumber min="0" style={{ width: '100%' }} placeholder="Enter deal period"/>
                </Form.Item>
                <Form.Item label="Max months">
                    <InputNumber min="0" style={{ width: '100%' }} placeholder="Enter deal period"/>
                </Form.Item>
                <Form.Item label="Interest" >
                    <InputNumber min="0" style={{ width: '100%' }} placeholder="Enter interes rate"/>
                </Form.Item>
                <Form.Item wrapperCol={{ span: 14, offset: 4 }}>
                    <Button type="primary">Add New Plan</Button>
                </Form.Item>
                
            </Form>
        </Card>
    )
}

export default CreatePlanForm;