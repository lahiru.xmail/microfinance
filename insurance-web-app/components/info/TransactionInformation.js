import React from 'react'
import { Table, Tag, Card } from 'antd';

function TransactionInformationTable() {

    const columns = [
        { title: 'Attribute', dataIndex: 'attribute', key: 'attribute', width: '20%' },
        { title: 'Description', dataIndex: 'description', key: 'description' }
    ];

    const data = [
        {
            attribute : 'From',
            description: 'Address of the sender.'
        },
        {
            attribute : 'To',
            description: 'Address of the receiver. Null when its a contract creation transaction.'
        },
        {
            attribute : 'Status',
            description: <><Tag color="green"> TRUE </Tag> if the transaction was 
                successful, <Tag color="red"> FALSE </Tag> if the EVM reverted the transaction.</>
        },
        {
            attribute : 'Contract address',
            description: 'The contract address created, if the transaction was a contract creation, otherwise Null'
        },
        {
            attribute : 'Value',
            description: 'Value transferred in wei.'
        },
        {
            attribute : 'Gas',
            description: 'Gas provided by the sender.'
        },
        {
            attribute : 'Gas price',
            description: 'Gas price provided by the sender in wei.'
        },
        {
            attribute : 'Gas used',
            description: 'The amount of gas used by this specific transaction alone.'
        },
        {
            attribute : 'Cumulative gas used',
            description: 'The total amount of gas used when this transaction was executed in the block.'
        },
        {
            attribute : 'Nonce',
            description: 'The number of transactions made by the sender prior to this one.'
        }
    ]

    return (
        <>
            <Card title="Transaction information" lg={18} xl={18} xxl={18}>
                <Table columns={columns} dataSource={data} pagination={false} size='small'/>
            </Card>
        </>
    )
}

export default TransactionInformationTable;