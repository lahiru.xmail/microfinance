import React, { useState, useContext } from 'react'
import { Table, Tag, Space, Typography, Card, Modal, Divider } from 'antd';
import AuthContext from '../stores/authContext';

function DealsTable() {

    const { user, login } = useContext(AuthContext);

    const [isModalVisible, setIsModalVisible] = useState(false);
    const [editId, setEditId] = useState(-1);

    const showModal = (value) => {
        setEditId(value);
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
            render: text => text,
        },
        {
            title: 'Amount',
            dataIndex: 'amount',
            key: 'amount',
        },
        {
            title: 'Period',
            dataIndex: 'period',
            key: 'period',
        },
        {
            title: 'Initial Payment',
            key: 'interest',
            dataIndex: 'interest'
        },
        {
            title: 'Final Payment',
            key: 'borrower',
            dataIndex: 'borrower',
            render: (text, record) => (
                <Space size="middle">
                    8263746
                    
                    <Tag color='geekblue' key='Accepted'>
                        {'Accepted'.toUpperCase()}
                    </Tag>
                </Space>
            ),
        },
        {
            title: 'Insurer',
            key: 'insurer',
            dataIndex: 'insurer',
            render: (record) => (
                <Space size="middle">
                    8263746
                </Space>
            ),
        },
        {
            title: 'Insurance Approval',
            key: 'insurance-approval',
            dataIndex: 'insuranceStatus',
            render: (record) => {
                let color = 'green'
                if (record == 'pending') {
                    color = 'geekblue';
                }
                else if (record == 'rejected') {
                    color = 'volcano'
                }
                else {
                    color = 'green'
                }
                return (
                <Tag color={color} key='Accepted'>
                    {record.toUpperCase()}
                </Tag>
            )},
        },
        {
            title: 'Insurer Fee',
            key: 'fee',
            dataIndex: 'fee',
        },
    
    
    ];
    
    const data = [
        {
            key: '1',
            id: '1',
            name: 'John Brown',
            amount: '100',
            period: '10 months',
            interest: 13,
            fee: 2,
            insurer: '', 
            insuranceStatus : 'accepted',
            tags: ['pending'],
        },
        {
            key: '2',
            id: '2',
            name: 'Jim Green',
            amount: '200',
            period: '18 months',
            interest: 12,
            fee: 3,
            insuranceStatus : 'pending',
            tags: ['approved'],
        },
        {
            key: '3',
            id: '3',
            name: 'Joe Black',
            amount: '400',
            period: '24 months',
            interest: 10,
            fee: 4,
            insuranceStatus : 'pending',
            tags: ['rejected'],
        },
    ];
    
    if (user.role == 'insurance') {
        columns.splice(1, 0 , {
            title: 'Broker Name',
            dataIndex: 'name',
            key: 'name'
        })
    
        columns.push({
            title: 'Action',
            dataIndex: '',
            key: 'x',
            render: (record) => 
                record.insuranceStatus == 'pending' ? <span>
                        <a href="javascript:void(0);" onClick={() => showModal(record.id)}>Approve</a>
                        <Divider type="vertical" />
                        <a href="javascript:;" style={{ color: 'red' }}>Reject</a>
                    </span> : null
        })
    }

    return (
        <>
            <Card title="Current Deals">
                <Table pagination={false} columns={columns} dataSource={data} />
            </Card>
            <Modal title={`Edit Policy ID  ${editId}`} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                <p>Edit {editId}</p>
            </Modal>
        </>
    )
}

export default DealsTable;