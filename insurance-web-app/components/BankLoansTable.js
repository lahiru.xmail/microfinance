import React, { useState } from 'react'

import { Table, Tag, Typography, Card } from 'antd';

const { Title } = Typography;

const loanColumns = [
    {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
        render: text => text,
    },
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: 'Amount',
        dataIndex: 'amount',
        key: 'amount',
    },
    {
        title: 'Period',
        dataIndex: 'period',
        key: 'period',
    },
    {
        title: 'Interest %',
        key: 'interest',
        dataIndex: 'interest'
    },
    {
        title: 'Status',
        key: 'tags',
        dataIndex: 'tags',
        render: tags => (
            <>
                {tags.map(tag => {
                    let color = tag.length > 5 ? 'geekblue' : 'green';
                    if (tag === 'pending') {
                        color = 'geekblue';
                    }
                    else if (tag == 'rejected') {
                        color = 'volcano'
                    }
                    else {
                        color = 'green'
                    }
                    return (
                        <Tag color={color} key={tag}>
                            {tag.toUpperCase()}
                        </Tag>
                    );
                })}
            </>
        ),
    }
];

const loanData = [
    {
        key: '1',
        id: '1',
        name: 'John Brown',
        amount: '150',
        period: '12 months',
        interest: 13,
        tags: ['pending'],
    },
    {
        key: '2',
        id: '2',
        name: 'Jim Green',
        amount: '200',
        period: '24 months',
        interest: 12,
        tags: ['approved'],
    },
    {
        key: '3',
        id: '3',
        name: 'Joe Black',
        amount: '500',
        period: '36 months',
        interest: 10,
        tags: ['rejected'],
    },
];

function BankLoansTable() {

    return (
        <Card title="Current Loans">
            <Table pagination={false} columns={loanColumns} dataSource={loanData} />
        </Card>
    )
}

export default BankLoansTable;