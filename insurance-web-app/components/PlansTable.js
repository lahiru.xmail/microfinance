import React, { useState, useContext, useEffect } from 'react'
import { Table, Tag, Space, Typography, Card, Button, Divider, Modal } from 'antd';
import AuthContext from '../stores/authContext';

function PlansTable() {

    const { Title } = Typography;
    const { user, login } = useContext(AuthContext);

    const [isModalVisible, setIsModalVisible] = useState(false);
    const [editId, setEditId] = useState(-1);

	const [data, setData] = useState('')

    const showModal = (value) => {
        setEditId(value);
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

	const fetchPosts = async () =>{
        const plansResponse = await fetch('http://localhost:9092/policy-plans');
        const plans = await plansResponse.json()

		setData([])
		for(let i=0; i<plans.length; i++)
            {
                let row = {
                key: plans[i]._id,
                id: plans[i]._id,
                amount: plans[i].loanAmount,
                period: plans[i].months,
                initialPayment: plans[i].initialPayment,
				finalPayment: plans[i].finalPayment
                }

                console.log(row)

                setData((prev) => {
                    return [...prev, row]
                })
            }
        console.log(plans);
    }

	useEffect(() => {
		fetchPosts();
	}, [])

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
            render: text => text,
        },
        {
            title: 'Amount',
            dataIndex: 'amount',
            key: 'amount',
        },
        {
            title: 'Period',
            dataIndex: 'period',
            key: 'period',
        },
        {
            title: 'Initial Payment',
            key: 'initialPayment',
            dataIndex: 'initialPayment',
        },
        {
            title: 'Final Payment',
            key: 'finalPayment',
            dataIndex: 'finalPayment',
        },
    ];

    if (user.role == 'insurance') {
        columns.push({
            title: 'Action',
            dataIndex: '',
            key: 'x',
            render: (record) => (
                // <Space size="small">
                // <Button type="text" style={{color: 'blue'}}>Edit {record.id}</Button>
                // <Button type="text" danger>Delete {record.id}</Button>
                // </Space>),
                <span>
                    <a href="javascript:void(0);" onClick={() => showModal(record.id)}>Edit</a>
                    <Divider type="vertical" />
                    <a href="javascript:;" style={{ color: 'red' }}>Delete</a>
                </span>
            )
        })
    }

    return (
        <>
            <Card title="Insurance Policy Plans" extra={<a href="javascript:void(0);" onClick={() => fetchPosts()}>Refresh</a>}  >
                <Table columns={columns} dataSource={data} />
            </Card>
            <Modal title={`Edit Policy ID  ${editId}`} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                <p>Edit {editId}</p>
            </Modal>
        </>
    )
}

export default PlansTable;