import React, { useState, useContext, useEffect } from 'react'

import { Table, Tag, Typography, Card, Divider, message, Modal } from 'antd';
import AuthContext from '../stores/authContext';

function PoliciesTable() {

    const { Title } = Typography;
    const { user, login, web3, InsurancePolicy  } = useContext(AuthContext);

    const [editId, setEditId] = useState('')
    const [claimId, setClaimId] = useState('')
    const [isModalVisible, setIsModalVisible] = useState('')
    const [isClaimModalVisible, setIsClaimModalVisible] = useState('')

    const showModal = (value) => {
        setEditId(value);
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const showClaimModal = (value) => {
        setClaimId(value);
        setIsClaimModalVisible(true);
    };

    const handleClaimOk = () => {
        setIsClaimModalVisible(false);
    };

    const handleClaimCancel = () => {
        setIsClaimModalVisible(false);
    };

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
            render: text => text,
        },
        {
            title: 'Borrower',
            dataIndex: 'borrower',
            key: 'borrower',
        },
        {
            title: 'Broker',
            dataIndex: 'broker',
            key: 'broker',
        },
        {
            title: 'Amount',
            dataIndex: 'amount',
            key: 'amount',
        },
        {
            title: 'Period',
            dataIndex: 'period',
            key: 'period',
        },
        {
            title: 'Loan ID',
            key: 'loanId',
            dataIndex: 'loanId'
        },
        {
            title: 'Plan ID',
            key: 'planId',
            dataIndex: 'planId'
        },
        {
            title: 'Insurance Fee',
            key: 'cover',
            dataIndex: 'cover'
        },
        {
            title: 'Status',
            key: 'status',
            dataIndex: 'status',
            render: tag => {
                // REQUESTED, APPROVED, REJECTED, ONGOING, CLAIMED, CLOSE 
                let color = '';
                let state = ''
                if (tag == 0) {
                    color = 'geekblue';
                    state = 'REQUESTED' 
                }
                else if (tag == 'rejected') {
                    color = 'volcano'
                }
                else {
                    color = 'green'
                }
                return (
                    <Tag color={color} key={tag}>
                        {state}
                    </Tag>
                );
            }
        },
        
    ];

    if (user.role == 'borrower') {
        // columns.splice(1, 0 , {
        //     title: 'Name',
        //     dataIndex: 'name',
        //     key: 'name'
        // })

        columns.push({
            title: 'Action',
            dataIndex: '',
            key: '',
            render: (record) => 
                record.status == 0 ? <span>
                        <a href="javascript:void(0);" onClick={() => showModal(record.id)}>Sign</a>
                    </span> : null
                // <Space size="small">
                // <Button type="text" style={{color: 'blue'}}>Edit {record.id}</Button>
                // <Button type="text" danger>Delete {record.id}</Button>
                // </Space>),
        })
    }

    if (user.role == 'bank') {
        // columns.splice(1, 0 , {
        //     title: 'Name',
        //     dataIndex: 'name',
        //     key: 'name'
        // })

        columns.push({
            title: 'Action',
            dataIndex: '',
            key: '',
            render: (record) => 
                record.status == 0 ? <span>
                        <a href="javascript:void(0);" onClick={() => showClaimModal(record.id)}>Claim</a>
                    </span> : null
                // <Space size="small">
                // <Button type="text" style={{color: 'blue'}}>Edit {record.id}</Button>
                // <Button type="text" danger>Delete {record.id}</Button>
                // </Space>),
        })
    }

	if (user.role == 'insurance') {
        // columns.splice(1, 0 , {
        //     title: 'Name',
        //     dataIndex: 'name',
        //     key: 'name'
        // })

        columns.push({
            title: 'Action',
            dataIndex: '',
            key: '',
            render: (record) => 
                record.status == 0 ? <span>
                        <a href="javascript:void(0);" onClick={() => showModal(record.id)}>Approve</a>
                        <Divider type="vertical" />
                        <a href="javascript:;" style={{ color: 'red' }}>Reject</a>
                    </span> : null
                // <Space size="small">
                // <Button type="text" style={{color: 'blue'}}>Edit {record.id}</Button>
                // <Button type="text" danger>Delete {record.id}</Button>
                // </Space>),
        })
    }

    const [data, setData] = useState([])

    const getPolicies = async () =>{
        try{
            const accounts = await window.ethereum.enable();
            console.log(accounts[0]);
            const response = await InsurancePolicy.methods.getAllPolicies().call();

            setData([]);

            for(let i=0; i<response.length; i++)
            {
                let row = {
                    key: response[i].id,
                    id: response[i].id,
                    amount: response[i].amount,
                    period: response[i].months,
                    planId: response[i].planId,
                    loanId: response[i].loanId,
                    cover: response[i].cover,
                    borrower: response[i].borrower,
                    broker: response[i].broker,
                    status: response[i].state,
                    borrowerSigned: response[i].isBorrowerSigned,
                    insuranceApprove: response[i].insuranceApprove
                    }

                console.log(row)

                setData((prev) => {
                    return [...prev, row]
                })
            }
            console.log(response)

        }catch(err){
            console.log(err)
            message.error('Error occured while loading plans');
        }
    }

    useEffect(() => {
        getPolicies();

        let emitter = InsurancePolicy.events.insurancePolicyRequest({fromBlock: 'latest'}, (error, data) => {
            console.log(data.returnValues);
            let result = data.returnValues

            let row = {
                key: result.id,
                id: result.id,
                amount: result.amount,
                period: result.months,
                planId: result.planId,
                loanId: result.loanId,
                cover: result.cover,
                borrower: result.borrower,
                broker: result.broker,
                status: result.state,
                borrowerSigned: result.isBorrowerSigned,
                insuranceApprove: result.insuranceApprove
                }

                console.log(row)

                setData((prev) => {
                    return [...prev, row]
                })
        })

        return () => {
            emitter.unsubscribe();
        }
    }, [])

    return (
        <>
            <Card title="Current Policies" extra={<a href="javascript:void(0);" onClick={() => getPolicies()}>Refresh</a>} >
                <Table pagination={true} columns={columns} dataSource={data} />
            </Card>
            <Modal title={`Sign Insurance Policy  ${editId}`} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                <p>Sign {editId}</p>
            </Modal>  
            <Modal title={`Claim Insurance Policy  ${claimId}`} visible={isClaimModalVisible} onOk={handleClaimOk} onCancel={handleClaimCancel}>
                <p>Claim {claimId}</p>
            </Modal>   
        </>
    )
}

export default PoliciesTable;