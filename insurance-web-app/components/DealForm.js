import React, { useState } from 'react'
import {
    Form,
    Input,
    Button,
    Radio,
    Select,
    Cascader,
    DatePicker,
    InputNumber,
    TreeSelect,
    Switch, Card
} from 'antd';

function DealForm() {
    const [componentSize, setComponentSize] = useState('default');

    const onFormLayoutChange = ({ size }) => {
        setComponentSize(size);
    };

    return (
        <Card title="Create Deal" style={{ margin: '0px 12px 12px 0px' }}>
            <Form
                labelCol={{
                    span: 4,
                }}
                wrapperCol={{
                    span: 18,
                }}
                layout="horizontal"
                initialValues={{
                    size: componentSize,
                }}
                onValuesChange={onFormLayoutChange}
                size={componentSize}
            >
                {/* <Form.Item label="Form Size" name="size">
                    <Radio.Group>
                        <Radio.Button value="small">Small</Radio.Button>
                        <Radio.Button value="default">Default</Radio.Button>
                        <Radio.Button value="large">Large</Radio.Button>
                    </Radio.Group>
                </Form.Item> */}
                <Form.Item label="Amount">
                    <InputNumber min="0" style={{ width: '100%' }} placeholder="Enter amount"/>
                </Form.Item>
                <Form.Item label="Period">
                    <InputNumber min="0" style={{ width: '100%' }} placeholder="Enter deal period"/>
                </Form.Item>
                <Form.Item label="Interest" >
                    <InputNumber min="0" style={{ width: '100%' }} placeholder="Enter interes rate"/>
                </Form.Item>
                <Form.Item label="Borrower">
                    <Input placeholder="Enter borrower address"/>
                </Form.Item>
                <Form.Item label="Insurer">
                    <Input placeholder="Enter insurer address"/>
                </Form.Item>
                <Form.Item label="Insurance">
                    <InputNumber min="0" style={{ width: '100%' }} placeholder="Enter insurer fee"/>
                </Form.Item>
                <Form.Item wrapperCol={{ span: 14, offset: 4 }}>
                    <Button type="primary">Create deal</Button>
                </Form.Item>
            </Form>
        </Card>
    )
}

export default DealForm;