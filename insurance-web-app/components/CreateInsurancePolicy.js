import React, { useState, useContext, useEffect } from 'react'
import { Table, Tag, Space, Typography, Card, Form, InputNumber, Input, Button, message } from 'antd';
import AuthContext from '../stores/authContext';

const { Title } = Typography;

function CreateInsurancePolicy() {

    const [componentSize, setComponentSize] = useState('default');
    const [admin, setAdmin] = useState('')

    const { user, login, BankPlans, InsurancePolicy  } = useContext(AuthContext);

    const [amount, setAmount] = useState('')
    const [period, setPeriod] = useState('')
    const [planId, setPlanId] = useState('')
    const [loanId, setLoanId] = useState('')
    const [cover, setCover] = useState('')
    const [borrower, setBorrower] = useState('')

    const onFormLayoutChange = ({ size }) => {
        setComponentSize(size);
    };

    const createInsuranceRequest = async (values) => {
        // e.preventDefault()
        console.log(values);
        try{
            const accounts = await window.ethereum.enable();
            console.log(accounts[0])
            
            console.log(amount);
            console.log(period);
            console.log(loanId);
            console.log(cover);
            console.log(planId);
            console.log(borrower);

            const response = await InsurancePolicy.methods.applyInsurance(values.LoanAmount, period, cover, planId, loanId, borrower).send({from: accounts[0]})
            console.log(response)
            message.success('New policy request created successfully');
        }catch(err){
            message.error('Error creating policy request');
            console.log(err)
        }
    }

    useEffect (async () => {
        try{
            const a = await InsurancePolicy.methods.getAdmin().call();
            console.log(a);
            setAdmin(a)
        }
        catch(err){
            console.log(err)
        }
        
    })
    
    return (
        <Card title="Insurance Policy Request">
            <Form
                labelCol={{
					lg: 4,
					xl: 3,
					xxl: 3,
				}}
				wrapperCol={{
					lg: 16,
					xl: 14,
					xxl: 12,
				}}
                layout="horizontal"
                initialValues={{
                    size: componentSize,
                }}
                onValuesChange={onFormLayoutChange}
                size={componentSize}
                labelAlign="left"
                onFinish={createInsuranceRequest}
            >
                <Form.Item label="Loan Amount" name="LoanAmount" rules={[{ required: true, message: 'Please input Loan amount!' }]}>
                    <InputNumber min="0" style={{ width: '100%' }} placeholder="Enter amount" value={amount}/>
                </Form.Item>
                <Form.Item label="Period">
                    <InputNumber min="0" style={{ width: '100%' }} placeholder="Enter loan period"
                    value={period} onChange={(e) => setPeriod(e)}/>
                </Form.Item>
                <Form.Item label="Loan ID" >
                    <InputNumber min="0" style={{ width: '100%' }} placeholder="Enter plan id"
                    value={loanId} onChange={(e) => setLoanId(e)}/>
                </Form.Item>
                <Form.Item label="Insurance fee" >
                    <InputNumber min="0" style={{ width: '100%' }} placeholder="Enter insurance fee"
                    value={cover} onChange={(e) => setCover(e)}/>
                </Form.Item>
                <Form.Item label="Insurance Plan ID" >
                    <InputNumber min="0" style={{ width: '100%' }} placeholder="Enter plan id"
                    value={planId} onChange={(e) => setPlanId(e)}/>
                </Form.Item>
                <Form.Item label="Borrower" >
                    <Input min="0" style={{ width: '100%' }} placeholder="Enter plan id"
                    value={borrower} onChange={(e) => setBorrower(e.target.value)}/>
                </Form.Item>
                <Form.Item wrapperCol={{
					lg: { span: 14, offset: 4 },
					xl: { span: 14, offset: 3 },
					xxl: { span: 14, offset: 3 } }}
                >
                    {/* <Button type="primary" onClick={(e) => createInsuranceRequest(e)}>Request insurance policy</Button> */}
                    <Button type="primary" htmlType="submit">Request insurance policy</Button>
                </Form.Item>
            </Form>
        </Card>
    )
}

export default CreateInsurancePolicy;