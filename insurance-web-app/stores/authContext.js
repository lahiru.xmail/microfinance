import { createContext, useEffect, useState } from 'react'
import Web3 from 'web3'

import InsurancePolicyBuild  from '../../blockchain/build/contracts/InsurancePolicy.json';
import MicroTokenBuild from '../../blockchain/build/contracts/MicroToken.json';
import UserIdentityBuild from '../../blockchain/build/contracts/UserIdentity.json';

const AuthContext = createContext({
    user: null,
    userRole: null,
    login: () => {}
})

export const AuthContextProvider = ({children}) => {
    const users = [
        {
            name: 'Leonard Hofstadter',
            role: 'broker',
            color: '#87d068'
        },
        {
            name: 'Sheldon Cooper',
            role: 'bank',
            color: '#8193E7'
        },
        {
            name: 'Rajesh Koothrappali',
            role: 'borrower',
            color: '#F3D377'
        },
        {
            name: 'Howard Wolowitz',
            role: 'insurance',
            color: '#CCDAD5'
        }
    ]

    const [user, setUser] = useState(users[0]);

    const login = (role) =>{
        if (role == 'broker')
        {
            setUser(users[0])
        }
        else if (role == 'bank')
        {
            setUser(users[1])
        }
        else if (role == 'borrower')
        {
            setUser(users[2])
        }
        else if (role == 'insurance')
        {
            setUser(users[3])
        }
    }

    const web3 = new Web3(Web3.givenProvider || 'http://127.0.0.1:7545');

    // Smart Contract Addresses
    const insurancePolicyAddress = InsurancePolicyBuild.networks[5777].address;
    const microTokenAddress = MicroTokenBuild.networks[5777].address;
	const userIdentityAddress = UserIdentityBuild.networks[5777].address;

	const UserIdentity = new web3.eth.Contract(UserIdentityBuild.abi, userIdentityAddress);
	const MicroToken = new web3.eth.Contract(MicroTokenBuild.abi, microTokenAddress);
    const InsurancePolicy = new web3.eth.Contract(InsurancePolicyBuild.abi, insurancePolicyAddress);
    
    const context = { user, login, web3, InsurancePolicy, MicroToken }

    return(
        <AuthContext.Provider value={context}>
            {children}
        </AuthContext.Provider>
    )
}

export default AuthContext;