const mongoose = require('mongoose')

const PlanSchema = mongoose.Schema({
    months: {
        type: Number,
        required: true
    },
    loanAmount: {
        type: Number,
        required: true
    },
    initialPayment: {
        type: Number,
        required: true
    },
	finalPayment: {
        type: Number,
        required: true
    },
})

module.exports = mongoose.model('Plans', PlanSchema)